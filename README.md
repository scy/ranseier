# ranseier

_Router Advertisement Name Server Editing, Incredibly Easy. Really!_

## What

This is a user-space handler for a [Linux netfilter queue](https://netfilter.org/projects/libnetfilter_queue/) in order to modify the list of DNS servers embedded in an IPv6 Router Advertisement package.

netfilter queue allows you to add custom logic to a netfilter ruleset (as created using [nftables](https://netfilter.org/projects/nftables) or [iptables](https://netfilter.org/projects/iptables/)).
Basically, you tell your ruleset "hey, please send packets matching these criteria to a custom handler with ID 1234", and then some user-space program can come along and say "hi, I'm 1234, what have you got?"
The handler is then able to inspect, modify or drop these packets.

ranseier works on RDNSS blocks in router advertisements and will replace the list of DNS servers with one you specify.

## Why

The main use case of ranseier is to work around an IPv6 router you can't control or reconfigure, but also not replace.

For example, suppose your shitty ISP provides you with a cable router you basically have to use, but its configuration options are really limited.
In particular, it's impossible to add DNS aliases or configure a different DNS server.

Furthermore, suppose this ISP is so shitty that they only supply you with a `/64` subnet.
This means that you can't really create a "sane" subnet with your own router behind theirs.

My solution to this challenge was to set up a Raspberry Pi as the only device physically connected to the ISP router.
The Pi is creating a [layer 2 bridge](https://en.wikipedia.org/wiki/Bridging_%28networking%29) spanning the crappy router and all of the clients (e.g. using a second network adapter connected to a switch) in software.
It's now able to modify the router's advertisements before they reach the clients.

## Installation

ranseier depends on [NetfilterQueue](https://github.com/oremanj/python-netfilterqueue) and [Scapy](https://scapy.net/).
You'll also need a somewhat current version of Python.
Oh, and it only runs on Linux.

To install the dependencies on Debian, try something like

```sh
sudo apt install build-essential python3-dev libnetfilter-queue-dev

# We will run as root, so these should probably be installed as root.
sudo pip3 install NetfilterQueue scapy
```

## Running

An example invocation would be

```sh
sudo ./ranseier.py -v 1234 fe80::1234:56ff:fe78:acab 2001:4860:4860::8888
```

* `-v` will print a single character for each packet modified (`.`) or unmodified (`?`). Modification might fail if the packets don't have a RDNSS block or are no router advertisements at all, in which case you should probably check your netfilter ruleset.
* `1234` is the queue ID you'll be passing to netfilter.
* The additional arguments are the IPv6 addresses of the nameservers you'd like to advertise instead of the original ones.

Note that if you are using link-local addresses (like I am here), you may not specify a scope (like `%eth0`), because the packets are local to a certain link anyway.
If you need to modify packets on several links, you need to run several instances of ranseier.

It's also totally fine to use global-scope addresses.

This was just the first part, though!
You'll also need to use nftables or iptables to give ranseier something to work on.

An example using nftables:

```
#!/usr/sbin/nft -f

flush ruleset

define uplink = eth1

table bridge filter {
	chain forward {
		type filter hook forward priority filter; policy accept;

		iifname $uplink icmpv6 type nd-router-advert queue num 1234
	}
}
```

Note that once you define this rule, all packets matching it will go into the queue **and not come out of it again** unless they've been processed by user-space, i.e., ranseier.
This means that while ranseier is not running, there will probably be no router advertisements reaching your clients.
Depending on your setup, this might be a good thing, because it will break IPv6 instead of giving the wrong DNS servers to clients.

## About the Project

ranseier's official home is at <https://codeberg.org/scy/ranseier>.

If there are things that you'd like to contribute, feel free to open an issue, but be aware that I don't consider this project especially important or worth my time.
It helped me solve a problem, and I'm publishing it hoping that it will help solve someone else's, too.

* If you'd like to contribute documentation: I'll appreciate it.
* It's improbable, but not impossible, that feature requests will be implemented, but: I won't introduce things that are not in the "mess around with router advertisements" category. So if you want to use ranseier to change the contents of HTTP packets, feel free to build something like that based on my code, but I won't integrate it here.
* Also, I won't be able to help you with your individual setup. At least not in my spare time, but I'm available for freelance consulting.
