#!/usr/bin/env python3

from ipaddress import IPv6Address
from typing import List, Union

from netfilterqueue import NetfilterQueue, Packet
from scapy.layers.inet6 import ICMPv6NDOptRDNSS, IPv6


class Modifier:

    def __init__(self, nameservers: List[Union[str, IPv6Address]] = [],
                 dot: bool = False):
        if len(nameservers) < 1:
            raise ValueError("Currently, you need to specify at least one nameserver.")
        if len(nameservers) > 127:
            raise ValueError("You cannot specify more than 127 nameservers. What is wrong with you?")

        self._nslist = [
            str(IPv6Address(ns)).split("%")[0]
            for ns in nameservers
        ]
        self._nslen = 2 * len(self._nslist) + 1

        self._dot = bool(dot)

    def modify(self, packet: Packet):
        # Convert to Scapy object.
        orig_payload = packet.get_payload()
        top = pkt = IPv6(orig_payload)
        changed = False

        while pkt.payload:
            pkt = pkt.payload
            if type(pkt) is ICMPv6NDOptRDNSS:
                pkt.len = self._nslen
                pkt.dns = self._nslist
                changed = True

        if changed:
            # Re-calculate the checksum.
            del top.payload.cksum
            top = top.__class__(bytes(top))
            # Update the packet with our changes and let netfilter use the new one.
            new_payload = bytes(top)
            packet.set_payload(new_payload)
            if self._dot:
                print(".", end="", flush=True)
        elif self._dot:
            print("?", end="", flush=True)

        packet.accept()

    def modify_or_accept(self, packet: Packet):
        try:
            self.modify(packet)
        except:
            packet.accept()


if __name__ == "__main__":
    from argparse import ArgumentParser, ArgumentTypeError

    def valid_qid(qid: str) -> int:
        qid = int(qid)
        if 0 <= qid <= 65535:
            return qid
        raise ArgumentTypeError("must be between 0 and 65535, inclusive")


    parser = ArgumentParser(description=
        "User-space netfilter queue handler for replacing DNS servers in "
        "captured IPv6 Router Advertisements (RDNSS option) with user-defined "
        "ones.",
        epilog="Copyright 2022 Tim Weber <https://codeberg.org/scy/ranseier>")
    parser.add_argument("qid", metavar="queue_id", type=valid_qid,
        help="netfilter queue ID")
    parser.add_argument("nameservers", metavar="v6_addr", type=IPv6Address,
        nargs="+", help="IPv6 address to announce as DNS server")
    parser.add_argument("-v", "--verbose", action="count", default=0,
        help="print `.` for a modified packet, `?` for an unmodified one")

    args = parser.parse_args()

    m = Modifier(args.nameservers, dot=(args.verbose >= 1))

    nfq = NetfilterQueue()
    nfq.bind(args.qid, m.modify_or_accept)

    print("Running.")
    try:
        nfq.run()
    except KeyboardInterrupt:
        print("Interrupted, shutting down.")

    nfq.unbind()
